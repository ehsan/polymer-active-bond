# polymer-active-bond

Source codes and docs for the project of studying single and collective dynamics of active polymers, with the active force imposed along the contour of the polymer. The code uses [HOOMD-Blue](https://github.com/glotzerlab/hoomd-blue) for performing MD simulations.

## Single dynamics

The file `contour-pol-3D.py` runs the simulation for the polymer with the moving active patch, with the frequency of <img src="https://render.githubusercontent.com/render/math?math=1/dt_{a}">. The case of homogeneous active force along the whole polymer corresponds to <img src="https://render.githubusercontent.com/render/math?math=dt_{a} = 0">.

## Collective dynamics

The file `contour-pol-3d-collective.py` runs simulations for dense assemblies of active polymers, with the homogenous active force along the contour of polymer.
sssss
