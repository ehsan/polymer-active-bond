#! /usr/bin/env python3
# singularity exec --nv your_singularity_image python3 ./contour-pol-3D-collective.py -h #tells you about input options
# singularity exec --nv ehsan-singularity-hoomdv2.8.1-attlj.simg python3 ./contour-pol-3D-collective.py --N-pols 100 --pol-size 20 -D 100 --KH 0.5 --KT 0.005 --Fact -0.1 --epsilon 1 --rca -1 --run-time 1e7 --thermal-time 2e5 --dump-period 2e3 --restart-period 5e6 --nl cell --walls xzz --fg 2
#rca=-1 for attractive
import hoomd
import hoomd.md
import hoomd.ehsan_plugin as ehsanplugin
import saw, sbs_class
import numpy as np, argparse, os
import math
##################################
parser = argparse.ArgumentParser()
parser.add_argument('--N-pols', type=int, help='number of polymers', dest='N_pols')
parser.add_argument('--pol-size', type=int, default=20, help='polymer size', dest='pol_size')
parser.add_argument('-D', type=int, help='simulation box size', default=500)
parser.add_argument('--KH', type=str, help='bending K', dest='KH', default='0.0')
parser.add_argument('--KT', type=str, default='0.0', help='KT', dest='KT')
parser.add_argument('--Fact', type=str, help='active force magnitude', dest='Fact', default='0.1')
parser.add_argument('--epsilon', type=float, help='epsilon in lj', dest='epsilon', default=1)
parser.add_argument('--sigma', type=float, help='sigma in lj', dest='sigma', default=1)
parser.add_argument('--rcut-att', '--rca', type=float, help='rcut for attractive lj, to disable it pass -1', default=-1, dest='r_cut_att')
parser.add_argument('--Fg', type=float, default=0, help='magnitude of the gravity force in z direction')
parser.add_argument('--walls', type=str, default='', help='define walls: pass x,y,z to have walls at all boundaries.')
parser.add_argument('--run-time', type=float, default=1e7, dest='run_time')
parser.add_argument('--thermal-time', type=float, default=5e5, dest='thermal_time')
parser.add_argument('--dump-period', type=float, default=1e3, dest='dump_period')
parser.add_argument('--restart-period', type=float, default=1e6, dest='restart_period')
parser.add_argument('--nl', type=str, default='cell')
parser.add_argument('--simid', type=str, help='simulation id or meta information', default='id0')
args = parser.parse_args()

N_beads = args.pol_size
N_pols = args.N_pols
D = args.D
K_H = float(args.KH)
box = hoomd.data.boxdim(D, D, D)
particle_types = ['monomer']
bond_types = ['polymer']
angle_types = ['harmonic', 'contour']
walls = args.walls.split(',')
# repulsive LJ:
epsilon = 1.0
sigma = 1.0
r_cut = sigma*2**(1./6.)
####################################
# Simulation parameters #
####################################
np.random.seed(None)
dt = 0.01
KT = float(args.KT)
gamma = 2 # damping constant
A = 2 #parameters of dpd potential
Fact = float(args.Fact) # active force
dump_period = int(args.dump_period)
restart_period = int(args.restart_period)
dump_fname = 'traj-Npol{}-polSize{}-D{}-A{}-KT{}-bendingK{}-simid{}.gsd'.format(N_pols, N_beads, D, A, args.KT, args.KH, args.simid)
restart_fname = 'restarts/restart-Npol{}-polSize{}-D{}-A{}-KT{}-bendingK{}-simid{}.gsd'.format(N_pols, N_beads, D, A, args.KT, args.KH, args.simid)
thermal_time = int(args.thermal_time)
run_time = int(args.run_time)
snap_args = {'N':0 , 'box': box, 'particle_types': particle_types,
                'bond_types': bond_types, 'angle_types': angle_types}

####################################
# Initialization #
###################################

sbs = sbs_class.sbs(hoomd, mode='gpu')
sbs.init_world(mode='gpu')
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True  # flag the simulation mood!
else:
    restart = False
    snapshot = hoomd.data.make_snapshot(**snap_args)
    # Active polymers
    for pol_id in range(N_pols):
        pol0 = dict(size=N_beads, bonds_typeid=0, particles_typeid=0,
                    pbc=np.array([False, False, False]), angles_typeid=0,
                    charge=pol_id)
        sbs.add_random_polymer(snapshot, L=np.array([D, D, D])*0.9, **pol0)

    # setting the rigidity
    N_angles_old = snapshot.angles.N
    snapshot.angles.resize(snapshot.angles.N*2)
    snapshot.angles.group[N_angles_old:] = snapshot.angles.group[:N_angles_old]
    snapshot.angles.typeid[N_angles_old:] = 1
    system = hoomd.init.read_snapshot(snapshot)
all_particles = hoomd.group.all()

####################################
# Simulation setup #
####################################

if args.nl == 'tree':
    nl = hoomd.md.nlist.tree(check_period=1)
elif args.nl == 'cell':
    nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.6)

####################################
# Bonds and the soft-core potential (DPD) #
####################################

dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.0*KT, seed=1)
dpd.pair_coeff.set(particle_types, particle_types, A=A, gamma=2.0)

fene = hoomd.md.bond.fene()
fene.bond_coeff.set('polymer', k=30.0, r0=1.5*sigma, sigma=sigma, epsilon=1)

contour_force = ehsanplugin.angle.contour()
contour_force.angle_coeff.set('contour', k=1, t0=Fact)
contour_force.angle_coeff.set('harmonic', k=0, t0=0)
contour_force.disable() #active force disabled during the creation of chains.

harmonic=hoomd.md.angle.table(1000)
def cosinepot(theta, kappa, theta_0):
    V =  kappa * (math.cos(theta)-math.cos(theta_0)); #force potential
    T = kappa * math.sin(theta); #torque
    return (V, T)

#angletable = angle.table(width=1000)
harmonic.angle_coeff.set('harmonic', func=cosinepot, coeff=dict(kappa=K_H, theta_0=np.pi))
harmonic.angle_coeff.set('contour', func=cosinepot, coeff=dict(kappa=0, theta_0=0))
#harmonic = hoomd.md.angle.harmonic()
#harmonic.angle_coeff.set('harmonic', k=K_H, t0=np.pi)  #bending potential parameters
#harmonic.angle_coeff.set('contour', k=0, t0=0)
harmonic.disable()#disabled during the creation of chains.

# LJ
print ("rep lj:", epsilon, sigma, r_cut)
print ("att lj:", args.epsilon, args.sigma, args.r_cut_att)

slj = ehsanplugin.pair.ehsanlj(r_cut=args.r_cut_att, nlist=nl)
rep_lj = hoomd.md.pair.lj(r_cut=r_cut, nlist=nl)
slj.pair_coeff.set(particle_types, particle_types, epsilon=args.epsilon,
                    sigma=args.sigma, r_cut=args.r_cut_att)
rep_lj.pair_coeff.set(particle_types, particle_types,
                    epsilon=epsilon, sigma=sigma, r_cut=r_cut)
slj.set_params(mode='shift')
rep_lj.set_params(mode='shift')
slj.disable()  # enable after removing overlaps!
rep_lj.disable()  # enable after removing overlaps!

# walls
if len(walls)>0:
    rep_walls = hoomd.md.wall.group()
    for wall_k in walls:
        orig = np.zeros(3)
        dim_id = 0*('x' in wall_k) + 1*('y' in wall_k) + 2*('z' in wall_k)
        orig[dim_id] = D*0.5
        rep_walls.add_plane(origin=orig, normal=-1*orig/(D*0.5))
        rep_walls.add_plane(origin=-orig, normal=1*orig/(D*0.5))
    lj_wall = hoomd.md.wall.lj(rep_walls, r_cut=2.**(1./6.))
    lj_wall.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0)#, r_extrap=1.1)

###################################
# removing overlaps #
###################################

if not(restart):
    fire = hoomd.md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    nve = hoomd.md.integrate.nve(group=all_particles)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()

###################################
# Main simulations #
###################################

dump_gsd = hoomd.dump.gsd(dump_fname, period=dump_period, group=all_particles,
        overwrite=not(restart), phase=0, dynamic=['attribute', 'topology', 'momentum', 'property'])
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all_particles, truncate=True, period=restart_period, phase=0)
dpd.disable()
rep_lj.enable
slj.enable() #shift of LJ potential
print ("bending stiffness and  \theta_0:", K_H , np.pi)
if K_H > 0:
    harmonic.enable()
if (args.Fg != 0):
    Fg = hoomd.md.force.constant(fvec=(0, 0, args.Fg), group=all_particles)
hoomd.md.integrate.mode_standard(dt=dt)
langevin = hoomd.md.integrate.langevin(group=all_particles, kT=1.0*KT, seed=np.random.randint(100000)+1)
langevin.set_gamma(particle_types, gamma=gamma)
if thermal_time > 0:
    hoomd.run_upto(thermal_time)
contour_force.enable()
hoomd.run_upto(run_time)
restart_gsd.write_restart()
