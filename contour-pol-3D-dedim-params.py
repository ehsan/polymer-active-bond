#! /usr/bin/env python3

import hoomd
import hoomd.md
import hoomd.ehsan_plugin as ehsanplugin
import sbs_class
import numpy as np
import argparse
import os
import math


def cosinepot(theta, kappa, theta_0):
    V = kappa*(math.cos(theta)-math.cos(theta_0))  # force potential
    T = kappa * math.sin(theta)  # torque
    return (V, T)


def my_callback(timestep):
    snap = system.take_snapshot(all=True)
    angles_active_typeid = snap.angles.typeid[snap.angles.N//2:]
    temp_angles_active_2d = angles_active_typeid.reshape((N_pols, Nangles1))
    accepted = (rng.random(N_pols) < LAMBDA)
    accepted_2d = np.repeat(accepted, Nangles1).reshape((N_pols, Nangles1))
    new_angles_active_2d = np.empty_like(temp_angles_active_2d)
    if 1 in angles_active_typeid:
        #angles_typeid = np.roll(angles_typeid, Na-2)
        new_angles_active_2d = np.roll(
            temp_angles_active_2d, Na-2, axis=1)*accepted_2d + temp_angles_active_2d*np.logical_not(accepted_2d)
    elif False:
        new_angles_active_2d[:] = 2
        new_angles_active_2d[:, :Na-2] = 1
    snap.angles.typeid[snap.angles.N//2:] = new_angles_active_2d.flatten()
    temp_active_particles = snap.angles.group[snap.angles.typeid == 1].flatten(
    )
    snap.particles.charge[:] = 0
    snap.particles.charge[temp_active_particles] = 1
    system.restore_snapshot(snap)
    return 0


##################################
parser = argparse.ArgumentParser()
parser.add_argument('--N-pols', type=int,
                    help='number of polymers', dest='N_pols')
parser.add_argument('-N', '--pol-size', type=int, default=20,
                    help='polymer size', dest='pol_size')
parser.add_argument(
    '--Na', type=int, help='Number of active particles', default=3, dest='Na')
parser.add_argument('-D', type=int, help='simulation box size', default=500)
parser.add_argument('--KT', type=str, default='0.0', help='KT', dest='KT')
parser.add_argument(
    '--Pe', type=str, help='Pe number: (Fact*R0*factor)/(kT), check --pe-factor', dest='Pe', default='1.0')
parser.add_argument(
    '--Pe-factor', type=int, help='Pe factor type: 0: N^2, 1: 1, default is 0', dest='Pe_factor', default=0)
parser.add_argument(
    '--lp', type=str, help='Persistence length: kappa/kT', dest='lp', default='0.0')
parser.add_argument('--dt-active', '--dt-a', type=str,
                    default='0', dest='dt_active')
parser.add_argument('--lambda', '-l', type=str,
                    default='1.0', dest='lambda_rate')
parser.add_argument('--run-time', type=float, default=1e7, dest='run_time')
parser.add_argument('--thermal-time', type=float,
                    default=5e5, dest='thermal_time')
parser.add_argument('--dump-period', type=float,
                    default=1e3, dest='dump_period')
parser.add_argument('--restart-period', type=float,
                    default=1e6, dest='restart_period')
parser.add_argument('--nl', type=str, default='cell')
parser.add_argument('--dt', type=float,
                    help='dt, default=0.01', default=0.01, dest='dt')
parser.add_argument('--gamma', type=float, default=1.0,
                    help='gamma', dest='gamma')
parser.add_argument('--epsilon', type=float,
                    help='epsilon in lj', dest='epsilon', default=1)
parser.add_argument('--sigma', type=float,
                    help='sigma in lj', dest='sigma', default=1)
parser.add_argument('--harmonic-A', '-A', type=float,
                    help='energy scale of harmonic potential (default=20)', dest='A', default=20)
parser.add_argument('--bond-K', '-K', type=float,
                    help='energy scale of the bond (default=30)', dest='K', default=30)
parser.add_argument('--bond-R0', '--R0', type=float,
                    help='length scale of the bond (default=30)', dest='R0', default=1.5)
parser.add_argument('--potentials', type=str,
                    help='lj or harmonic? default is lj', default='lj')
parser.add_argument('--polymer-type', '--pt', type=str,
                    help='type of polymers? linear (default) or ring', default='linear', dest='polymer_type')
parser.add_argument('--bond-type', type=str,
                    help='fene or harmonic? default is fene', default='fene', dest='bond_type')
parser.add_argument('--device', type=str,
                    help='cpu or gpu (default)', default='gpu')
parser.add_argument('--fname-prefix', type=str,
                    default='', dest='fname_prefix')
parser.add_argument('--integrator', type=str, default='langevin',
                    dest='integrator', help='langevin (default) or brownian')
parser.add_argument('--simid', type=str,
                    help='simulation id or meta information', default='id0')
args = parser.parse_args()
if args.polymer_type.lower() == 'linear':
    RING_POLYMER = False
else:
    RING_POLYMER = True
    # All beads are active, no need for callabck

N = N_beads = args.pol_size
Na = args.Na
LAMBDA = float(args.lambda_rate)
Nangles1 = N-2*(RING_POLYMER == False)
N_pols = args.N_pols
D = args.D
box = hoomd.data.boxdim(D, D, D)
particle_types = ['p%d' % i for i in range(N_pols)]
bond_types = ['polymer', 'active', 'active2'][:1]
angle_types = ['bending', 'contour', 'contouroff']
rng = np.random.default_rng(None)
####################################
## Simulation parameters ##
####################################
R0 = args.R0
np.random.seed(None)
KT = float(args.KT)
if args.Pe_factor == 0:
    PE_FACTOR = N**2
else:
    PE_FACTOR = 1
Pe = float(args.Pe)  # Pe = (N_ap/N_ap_tot)(Fact*R0*PE_FACTOR)/KT
lp = float(args.lp)  # lp = kappa/KT
if not(RING_POLYMER):
    # N_ap_tot = N-2
    # N_ap = Na-2
    Fact = -Pe*KT*(N-2)/(R0*(Na-2)*PE_FACTOR)
else:
    # N_ap_tot = N
    # N_ap = Na-2
    Fact = -Pe*KT*(N)/(R0*(Na-2)*PE_FACTOR)

print('Pe, Fact:', Pe, Fact)
kappa = lp*KT
print('Facot={}, kappa={}'.format(Fact, kappa))
gamma = args.gamma
t0 = gamma / KT  # unit of time
dt = args.dt*t0
A = args.A*KT
LJ = False
if args.potentials.lower() == 'lj':
    LJ = True
FENE = False
if args.bond_type.lower() == 'fene':
    FENE = True
# repulsive LJ:
sigma = args.sigma
epsilon = args.epsilon*KT
r_cut = sigma*2**(1./6.)
dump_period = int(float(args.dump_period)*t0/dt)
print('dump_period', dump_period)
restart_period = int(float(args.restart_period)*t0/dt)
fname_info = 'Npol{}-N{}-KT{}-Pe{}-lp{}-t_a{}-rt{}-dump{}-simid{}.gsd'.format(
    N_pols, N_beads, args.KT, args.Pe, args.lp, args.dt_active, args.run_time, args.dump_period, args.simid)
dump_fname = args.fname_prefix + 'dump-' + fname_info
restart_fname = args.fname_prefix + 'restart-' + fname_info
thermal_time = int(float(args.thermal_time)*t0/dt)
run_time = int(float(args.run_time)*t0/dt)
callback_period = int(float(args.dt_active)*t0/dt)
if Na == N:
    callback_period = -1

print('callback_period={}'.format(callback_period))
snap_args = {'N': 0, 'box': box, 'particle_types': particle_types,
             'bond_types': bond_types, 'angle_types': angle_types}

###################################
# Initialization #
###################################

sbs = sbs_class.sbs(hoomd, mode=args.device)
sbs.init_world(mode=args.device)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True  # flag the simulation mood!
else:
    restart = False
    snapshot = hoomd.data.make_snapshot(**snap_args)
    # Active polymers
    for pol_id in range(N_pols):
        pol0 = dict(size=N_beads, bonds_typeid=0, particles_typeid=pol_id, pbc=np.array(
            [False, False, False]), angles_typeid=0, dimensions=3)
        if not(RING_POLYMER):
            sbs.add_random_polymer(snapshot, L=np.array([D, D, D]), **pol0)
        else:
            sbs.add_random_ring_polymer(
                snapshot, L=np.array([D, D, D]), R0=args.R0, **pol0)

    # setting the rigidity
    N_angles_old = snapshot.angles.N
    snapshot.angles.resize(snapshot.angles.N*2)
    snapshot.angles.group[N_angles_old:] = snapshot.angles.group[:N_angles_old]
    # if callback_period > 0:
    if Na != N:
        angles1 = np.ones(N-2*(RING_POLYMER == False))*2
        angles1[:Na-2] = 1
        angles_active_all = np.tile(angles1, N_pols)
        angles_active_2d = angles_active_all.reshape(
            (N_pols, N-2*(RING_POLYMER == False)))

        #snapshot.angles.typeid[N_angles_old:] = 2
        #snapshot.angles.typeid[N_angles_old::N_beads-2] = 1
        snapshot.angles.typeid[N_angles_old:] = angles_active_all
    else:
        snapshot.angles.typeid[N_angles_old:] = 1
    angles_typeid = snapshot.angles.typeid
    active_particles = snapshot.angles.group[angles_typeid == 1].flatten()
    snapshot.particles.charge[:] = 0
    snapshot.particles.charge[active_particles] = 1
    system = hoomd.init.read_snapshot(snapshot)

all_particles = hoomd.group.all()

####################################
# Simulation setup #
####################################

if args.nl == 'tree':
    nl = hoomd.md.nlist.tree(check_period=1)
elif args.nl == 'cell':
    nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.6)

####################################
# Bonds and the soft-core potential (DPD) #
####################################

dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.0*KT, seed=1)
dpd.pair_coeff.set(particle_types, particle_types, A=0, gamma=2.0, r_cut=False)
for p0 in particle_types:
    dpd.pair_coeff.set(p0, p0, A=A, gamma=gamma, r_cut=1.0)

if FENE:
    fene = hoomd.md.bond.fene()
    fene.bond_coeff.set('polymer', k=args.K*KT/R0**2,
                        r0=R0*sigma, sigma=sigma, epsilon=1*KT)
else:
    harmonic_bond = hoomd.md.bond.harmonic()
    harmonic_bond.bond_coeff.set('polymer', k=args.K*KT, r0=R0)

contour_force = ehsanplugin.angle.contour()
contour_force.angle_coeff.set('contour', k=1, t0=Fact)
contour_force.angle_coeff.set('bending', k=-1, t0=0)
contour_force.angle_coeff.set('contouroff', k=-1, t0=0)
contour_force.disable()


bending = hoomd.md.angle.table(1000)
bending.angle_coeff.set('bending', func=cosinepot,
                        coeff=dict(kappa=kappa, theta_0=np.pi))
bending.angle_coeff.set('contour', func=cosinepot,
                        coeff=dict(kappa=0, theta_0=0))
bending.angle_coeff.set('contouroff', func=cosinepot,
                        coeff=dict(kappa=0, theta_0=0))
bending.disable()  # disabled during the creation of chains.

contouroff_force = hoomd.md.angle.cosinesq()
contouroff_force.angle_coeff.set('contour', k=0, t0=0)
contouroff_force.angle_coeff.set('contouroff', k=0, t0=0)
contouroff_force.angle_coeff.set('bending', k=0, t0=0)
contouroff_force.disable()

# LJ
if LJ:
    rep_lj = hoomd.md.pair.lj(r_cut=r_cut, nlist=nl)
    rep_lj.pair_coeff.set(particle_types, particle_types,
                          epsilon=epsilon, sigma=sigma, r_cut=False)
    for p0 in particle_types:
        rep_lj.pair_coeff.set(p0, p0, epsilon=epsilon,
                              sigma=sigma, r_cut=r_cut)
    rep_lj.set_params(mode='shift')
    rep_lj.disable()  # enable after removing overlaps!

###################################
# removing overlaps #
###################################

if not(restart):
    fire = hoomd.md.integrate.mode_minimize_fire(
        dt=0.001, ftol=1e-5, Etol=1e-7)
    nve = hoomd.md.integrate.nve(group=all_particles)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()
    # dpd.disable()

###################################
# Main simulations #
###################################
dump_gsd = hoomd.dump.gsd(dump_fname, period=dump_period, group=all_particles, overwrite=not(
    restart), phase=0, dynamic=['attribute', 'topology', 'momentum', 'property'])
restart_gsd = hoomd.dump.gsd(
    filename=restart_fname, group=all_particles, truncate=True, period=restart_period, phase=0)

if LJ:
    dpd.disable()
    rep_lj.enable()
if kappa > 0:
    bending.enable()

hoomd.md.integrate.mode_standard(dt=dt, aniso=False)
if 'lang' in args.integrator.lower():
    integrator = hoomd.md.integrate.langevin(
        group=all_particles, kT=KT, seed=np.random.randint(100000)+1)
elif 'brown' in args.integrator.lower():
    integrator = hoomd.md.integrate.brownian(
        group=all_particles, kT=KT, seed=np.random.randint(100000)+1)

for pt in particle_types:
    integrator.set_gamma(pt, gamma=gamma)

if thermal_time > 0:
    hoomd.run_upto(thermal_time)

contour_force.enable()
if callback_period > 0:
    change_angles = hoomd.analyze.callback(
        callback=my_callback, period=callback_period)

hoomd.run_upto(run_time)
restart_gsd.write_restart()
