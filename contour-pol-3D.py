#! /usr/bin/env python3

import hoomd
import hoomd.md
import hoomd.ehsan_plugin as ehsanplugin
import saw, sbs_class
import numpy as np, argparse, os, math


def cosinepot(theta, kappa, theta_0):
    V = kappa*(math.cos(theta)-math.cos(theta_0))  # force potential
    T = kappa * math.sin(theta)  # torque
    return (V, T)


def my_callback(timestep):
    snap = system.take_snapshot(all=True)
    angles_typeid = snap.angles.typeid
    if 1 in angles_typeid:
        # bonds_typeid = np.roll(bonds_typeid, 1)
        angles_typeid = np.roll(angles_typeid, 1)
    else:
        angles_typeid[np.arange(0, len(angles_typeid), N_beads-2)] = 1
    snap.angles.typeid[:] = angles_typeid
    active_particles = snap.angles.group[angles_typeid == 1].flatten()
    snap.particles.charge[:] = 0
    snap.particles.charge[active_particles] = 1
    system.restore_snapshot(snap)
    return 0
##################################
parser = argparse.ArgumentParser()
parser.add_argument('--N-pols', type=int, help='number of polymers', dest='N_pols')
parser.add_argument('--pol-size', type=int, default=20, help='polymer size', dest='pol_size')
parser.add_argument('-D', type=int, help='simulation box size', default=500)
parser.add_argument('--KH', type=str, help='bending K', dest='KH', default='0.0')
parser.add_argument('--KT', type=str, default='0.0', help='KT', dest='KT')
parser.add_argument('--Fact', type=str, help='Fact', dest='Fact', default='0.1')
parser.add_argument('--dt-active', '--dt-a', type=str, default='0', dest='dt_active')
parser.add_argument('--run-time', type=float, default=1e7, dest='run_time')
parser.add_argument('--thermal-time', type=float, default=5e5, dest='thermal_time')
parser.add_argument('--dump-period', type=float, default=1e3, dest='dump_period')
parser.add_argument('--restart-period', type=float, default=1e6, dest='restart_period')
parser.add_argument('--nl', type=str, default='cell')
parser.add_argument('--dt', type=float, help='dt, default=0.01', default=0.01, dest='dt')
parser.add_argument('--gamma', type=float, default=2.0, help='gamma', dest='gamma')
parser.add_argument('--epsilon', type=float, help='epsilon in lj', dest='epsilon', default=1)
parser.add_argument('--sigma', type=float, help='sigma in lj', dest='sigma', default=1)
parser.add_argument('--harmonic-A', '-A', type=float, help='energy scale of harmonic potential (default=20)', dest='A', default=20)
parser.add_argument('--harmonic-bond-K', '-K', type=float, help='energy scale of harmonic bond (default=30)', dest='K', default=30)
parser.add_argument('--potentials', type=str, help='lj or harmonic? default is lj', default='lj')
parser.add_argument('--device', type=str, help='cpu or gpu (default)', default='gpu')
parser.add_argument('--simid', type=str, help='simulation id or meta information', default='id0')
args = parser.parse_args()

callback_period = int(float(args.dt_active))
N_beads = args.pol_size
N_pols = args.N_pols
D = args.D
K_H = float(args.KH)
box = hoomd.data.boxdim(D,D,D)
particle_types = ['p%d'%i for i in range(N_pols)]
bond_types = ['polymer', 'active', 'active2'][:1]
angle_types = ['harmonic', 'contour', 'contouroff']

## repulsive LJ:
sigma = args.sigma
epsilon = args.epsilon
r_cut = sigma*2**(1./6.)
####################################
## Simulation parameters ##
####################################
np.random.seed(None)
dt = args.dt
KT = float(args.KT)
gamma = args.gamma
A = args.A
LJ = False
if args.potentials.lower()=='lj':
    LJ = True
Fact = float(args.Fact) # active force
dump_period = int(args.dump_period)
restart_period = int(args.restart_period)
#dump_fname = 'traj-Npol{}-polSize{}-D{}-A{}-KT{}-bendingK{}-dt_a{}.gsd'.format(N_pols, N_beads, D, A, args.KT, args.KH, args.dt_active)
#restart_fname = 'restarts/restart-Npol{}-polSize{}-D{}-A{}-KT{}-bendingK{}-dt_a{}.gsd'.format(N_pols, N_beads, D, A, args.KT, args.KH, args.dt_active)
dump_fname = 'traj-Npol{}-polSize{}-D{}-KT{}-bendingK{}-dt_a{}-simid{}.gsd'.format(N_pols, N_beads, D, args.KT, args.KH, args.dt_active, args.simid)
restart_fname = 'restarts/restart-Npol{}-polSize{}-D{}-KT{}-bendingK{}-dt_a{}-simid{}.gsd'.format(N_pols, N_beads, D, args.KT, args.KH, args.dt_active, args.simid)
thermal_time = int(args.thermal_time)
run_time = int(args.run_time)
snap_args = {'N':0 , 'box': box, 'particle_types': particle_types,
             'bond_types': bond_types, 'angle_types': angle_types}

###################################
# Initialization #
###################################

sbs = sbs_class.sbs(hoomd, mode=args.device)
sbs.init_world(mode=args.device)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True  # flag the simulation mood!
else:
    restart = False
    snapshot = hoomd.data.make_snapshot(**snap_args)
    # Active polymers
    for pol_id in range(N_pols):
        pol0 = dict(size=N_beads, bonds_typeid=0, particles_typeid=pol_id, pbc=np.array([False, False, False]), angles_typeid=0, dimensions=3)
        sbs.add_random_polymer(snapshot, L=np.array([D, D, D]), **pol0)

    # setting the rigidity
    N_angles_old = snapshot.angles.N
    snapshot.angles.resize(snapshot.angles.N*2)
    snapshot.angles.group[N_angles_old:] = snapshot.angles.group[:N_angles_old]
    if callback_period > 0:
        snapshot.angles.typeid[N_angles_old:] = 2
        snapshot.angles.typeid[N_angles_old::N_beads-2] = 1
    else:
        snapshot.angles.typeid[N_angles_old:] = 1
    system = hoomd.init.read_snapshot(snapshot)

all_particles = hoomd.group.all()

####################################
# Simulation setup #
####################################

if args.nl == 'tree':
    nl = hoomd.md.nlist.tree(check_period=1)
elif args.nl == 'cell':
    nl = hoomd.md.nlist.cell(check_period=1)
nl.set_params(r_buff=0.6)

####################################
# Bonds and the soft-core potential (DPD) #
####################################

dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.0*KT, seed=1)
dpd.pair_coeff.set(particle_types, particle_types, A=0, gamma=2.0, r_cut=False)
for p0 in particle_types:
    dpd.pair_coeff.set(p0, p0, A=A, gamma=2.0, r_cut=1.0)

if LJ:
    fene = hoomd.md.bond.fene()
    fene.bond_coeff.set('polymer', k=30.0, r0=1.5*sigma, sigma=sigma, epsilon=1)
else:
    harmonic_bond = hoomd.md.bond.harmonic()
    harmonic_bond.bond_coeff.set('polymer', k=args.K, r0=1.1)

contour_force = ehsanplugin.angle.contour()
contour_force.angle_coeff.set('contour', k=1, t0=Fact)
contour_force.angle_coeff.set('harmonic', k=-1, t0=0)
contour_force.angle_coeff.set('contouroff', k=-1, t0=0)
contour_force.disable()


harmonic = hoomd.md.angle.table(1000)
harmonic.angle_coeff.set('harmonic', func=cosinepot, coeff=dict(kappa=K_H, theta_0=np.pi))
harmonic.angle_coeff.set('contour', func=cosinepot, coeff=dict(kappa=0, theta_0=0))
harmonic.angle_coeff.set('contouroff', func=cosinepot, coeff=dict(kappa=0, theta_0=0))
harmonic.disable()#disabled during the creation of chains.

contouroff_force = hoomd.md.angle.cosinesq()
contouroff_force.angle_coeff.set('contour', k=0, t0=0)
contouroff_force.angle_coeff.set('contouroff', k=0, t0=0)
contouroff_force.angle_coeff.set('harmonic', k=0, t0=0)
contouroff_force.disable()

## LJ
if LJ:
    rep_lj = hoomd.md.pair.lj(r_cut=r_cut, nlist=nl)
    rep_lj.pair_coeff.set(particle_types, particle_types,
                        epsilon=epsilon, sigma=sigma, r_cut=False)
    for p0 in particle_types:
        rep_lj.pair_coeff.set(p0, p0, epsilon=epsilon, sigma=sigma, r_cut=r_cut)
    rep_lj.set_params(mode='shift')
    rep_lj.disable()  # enable after removing overlaps!

###################################
# removing overlaps #
###################################

if not(restart):
    fire = hoomd.md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    nve = hoomd.md.integrate.nve(group=all_particles)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()
    #dpd.disable()

###################################
# Main simulations #
###################################

dump_gsd = hoomd.dump.gsd(dump_fname, period=dump_period, group=all_particles, overwrite=not(restart), phase=0, dynamic=['attribute', 'topology', 'momentum', 'property'])
restart_gsd = hoomd.dump.gsd(filename=restart_fname, group=all_particles, truncate=True, period=restart_period, phase=0)

if LJ:
    dpd.disable()
    rep_lj.enable()
if K_H > 0:
    harmonic.enable()

hoomd.md.integrate.mode_standard(dt=dt)
langevin  = hoomd.md.integrate.langevin(group=all_particles, kT=1.0*KT, seed=np.random.randint(100000)+1)
for pt in particle_types:
    langevin.set_gamma(pt, gamma=gamma)
    langevin.set_gamma_r(pt, gamma_r=gamma)

if thermal_time > 0:
    hoomd.run_upto(thermal_time)

contour_force.enable()
if callback_period > 0:
    change_bonds = hoomd.analyze.callback(callback=my_callback, period=callback_period)

hoomd.run_upto(run_time)
restart_gsd.write_restart()
