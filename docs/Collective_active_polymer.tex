% ****** Start of file aipsamp.tex ******
%
%   This file is part of the AIP files in the AIP distribution for REVTeX 4.
%   Version 4.1 of REVTeX, October 2009
%
%   Copyright (c) 2009 American Institute of Physics.
%
%   See the AIP README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex  aipsamp
%  2)  bibtex aipsamp
%  3)  latex  aipsamp
%  4)  latex  aipsamp
%
% Use this file as a source of example code for your aip document.
% Use the file aiptemplate.tex as a template for your document.
%\documentclass[pre,showpacs,preprint]{revtex4}
%\documentclass[pre,showpacs,notitlepage]{revtex4-1}
%\documentclass[ aip,jmp, amsmath,amssymb,preprint]{revtex4-1}
\documentclass[pre,showpacs,notitlepage,twocolumn]{revtex4-1}
\pdfoutput=1
%\documentclass[prl,showpacs,twocolumn,superscriptaddress]{revtex4}

%\documentclass[prl,showpacs,preprint,superscriptaddress]{revtex4}

%\documentclass[a4paper,prl,twocolumn,superscriptaddress]{revtex4}

%\documentclass[a4paper,pre,preprint,superscriptaddress]{revtex4}

%\documentclass[pre,showpacs,preprint,superscriptaddress]{revtex4}

%\usepackage{hyperref}


\usepackage[colorlinks,linkcolor={blue},citecolor={blue},urlcolor={blue}]{hyperref}
%\usepackage{url}
%\usepackage{breakurl}

\usepackage{graphicx}

\usepackage{bm,amsmath}

\usepackage{latexsym}

\usepackage{verbatim}

\usepackage{color}

\usepackage{subfigure}

\usepackage{gensymb}

\usepackage{soul}

\usepackage[dvipsnames]{xcolor}

%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines
\newcommand{\sara}[1]{{\color{red} #1}}
\newcommand{\bR}{\mathbf{R}}
\renewcommand*\vec[1]{\mathbf{\bm{#1}}}
\begin{document}


 

%\preprint{AIP/123-QED}

\title[]{ Collective behavior of tangentially driven active polymers in 3D }% Force line breaks with \\
%Static scaling properties  of semiflexible crystallizable polymer melts
%\thanks{Footnote to title of article.}

\author{Sara Jabbari-Farouji $^{*}$}
 %\altaffiliation[Also at ]{Physics Department, XYZ University.}%Lines break automatically or can be forced with \\
\affiliation{Institute of Physics,  University of Amsterdam
}%
 \email{Correspondence to: s.jabbarifarouji@uva.nl}
%\altaffiliation{}%Lines break automatically or can be forced with \

\author{Ehsan Irani}

\author{Zahra Mokhtari}




%\author{C. Author}
% \homepage{http://www.Second.institution.edu/\simCharlie.Author.}
%\affiliation{%Second institution and/or address%\\This line break forced% with \\}%

 \date{\today}% It is always \today, today,
             %  but any date may be explicitly specified

%\pacs{ PACS}% PACS, the Physics and Astronomy
                             % Classification Scheme.

  \begin{abstract}
  
  Here, we investigate the collective dynamics of   dense systems of tangentially-driven active polymers in 3D applicable to a variety of biological organisms such as worms.
  An interesting question is how the mobility induced phase separation is affected by the chain bending stiffness. We focus on short un-entangled chains.
\end{abstract}


\keywords{Active Polymer}%Use showkeys class option if keyword
 

 


\maketitle

\section{Model definition and its dimensionless parameters}
In this section, we  describe the  active polymer model and its associated  dimensionless groups that will be varied throughout the simulations.

 \subsection{The tangentially driven active filament model}
 We adapt the tangentially-driven  polymer model used by Isele-Holder {et al.} \cite{Isele} and \cite{Prathyusha} to study collective dynamics of active semiflexible filaments.
The active filament  is modelled as a semiflexible linear polymer composed of a sequence of $N$ bead-like units connected via finitely extensible nonlinear elastic (FENE) springs under the influence of an active force of constant magnitude.  We consider $n_c$ active filaments in a 3D cubic box of length $L_{\text{box}}$ with periodic boundary conditions. All the filaments have the same degree of polymerization $N$ consisting of  beads with diameter $\sigma$, leading to an overall bead volume fraction $\phi=( \pi  N n_c \sigma^3)/(6L_{\text{box}}^3)=\frac{\pi}{6} \rho \sigma^3  $.   In experiments, filaments are surrounded by a fluid that mediates hydrodynamic interactions. However, for  sufficiently dense active polymer suspensions, the hydrodynamic interactions are screened and the surrounding fluid only provides the single-particle friction. Therefore, we focus on dry dense active polymeric systems  with $\phi > 0.5$. 
 
 The  dynamics of  each bead is then described by Langevin equation  of motion:
 %
\begin{equation}
\label{eq:Langevin}
m \ddot{\vec{r}}_i=-\gamma \dot{\vec{r}}_i- \sum_j \nabla_{\vec{r}_i} U(r_{ij})+\vec{f}^a_{i}+\vec{f}^{r}_{i}
 \end{equation}
%
in which $\vec{r}_i$ is the coordinates of the bead $i$ with the dots denoting
derivatives with respect to time. $m$ denotes the mass, $\gamma$ is the  effective friction coefficient of the bead. $r_{ij}=|\vec{r}_j-\vec{r}_i|$ denotes the distance between bead $i$ and $j$  and $U(r_{ij})$ is the potential energy of interparticle interactions. $\vec{f}^a_{i}$  and $\vec{f}^{r}_{i}$ are the active   and random thermal forces acting on the bead $i$, respectively.
  The total potential energy of a bead can be written as the sum of three contributions $U = U_{\text{FENE} }+ U_{\text{bend}}+U_{\text{WCA}}$, where $U_{\text{FENE}} $ is  the non-linear spring potential, $U_{\text{bend}}$ is bending potential controlling the angle between two consequent bonds, and $U_{WCA}$ corresponds to excluded volume potential. 


 The excluded volume interactions between any two beads, bonded or non-bonded,  at a distance $r$ are modelled  by
  the truncated and shifted Lennard-Jones potential, {\it i.e.}, Weeks-Chandler-Andersen (WCA) potential,
 %
\begin{equation}
U_{\text{WCA}}(r)=4  \epsilon \left[  (\frac{\sigma}{r})^{12} -(\frac{\sigma}{r})^6+\frac{1}{4}\right] \,   r< r_{cut}.
 \end{equation}
%
 where $\epsilon$ denotes the strength of pairwise interaction energy, and $r_{cut} = 2^{1/6} \sigma$ is the cutoff distance such that $U_{\text{WCA}}(r > r_{cut}) = 0$.
 The degree of bond stretching between any two connected
monomers of diameter $\sigma$ along a chain are controlled by the finitely extensible nonlinear elastic (FENE) potential 
 
  %
\begin{equation}
U_{\text{FENE}}(r)= -1/2 k_s R_0^2 \ln(1-(r/R_0)^2) \,   r < R_{0}.
 \end{equation}
%
 and $U_{\text{FENE}}(r > R_0) = \infty$. Here, $k_s$ is the stiffness of the non-linear spring and $R_0$ corresponds to the maximum bond length.
  
 The bending stiffness of the chain is controlled by the bending potential depending on the bond angle $\theta$ between the sequential bonds along a chain 
 %
\begin{equation}
U_{\text{bend}}(r)=\kappa (1-\cos \theta).
 \end{equation}
%
 where $\kappa$  gives the strength of the bending stiffness. Defining the unit tangent vector along the bond  connecting beads $i$ and $i + 1$  along a chain as  $\widehat{\vec{t}}_{i,i+1}= \vec{r}_{i,i+1}/r_{i,i+1}$, with $\vec{r}_{i,i+1}=\vec{r}_{i+1}-\vec{r}_{i}$, the angle  $\theta$ between two consequent  bonds intersecting at bead $i$ is obtained as $\theta= \cos^{-1} (\widehat{\vec{t}}_{i,i+1} \cdot \widehat{\vec{t}}_{i-1,i})$.    

 The active force on each bead $i$ is modeled as 
 %
\begin{equation}
\label{eq:activeforce}
\vec{f}^a_i=f^a \sara{\frac{ (\widehat{\vec{t}}_{i-1,i}+\widehat{\vec{t}}_{i,i+1})} {|\widehat{\vec{t}}_{i-1,i}+\widehat{\vec{t}}_{i,i+1}|} },
 \end{equation}
%
 which mimics a homogenous active driving along the chain. %produced by a homogeneous distribution of molecular motors. 
Here, $f^a$ is the strength of  active force and   the  orientation of force on end beads is parallel
to end bonds. Our choice of
active force in Eq.\eqref{eq:activeforce} is  identical to Ref.~\cite{Prathyusha} and slightly different than the active forces used in Refs.~\cite{Isele,active_priodic_force}, where the tangent vectors were not normalized. 
 $\vec{f}^r_i$  is a delta correlated random thermal force acting on particle $i$ with zero mean.
The viscous drag and the thermal noise are related through the fluctuation-dissipation relation
%
\begin{equation}
\label{eq:randomforce}
\langle \vec{f}^r_i(t) \cdot \vec{f}^r_j(t') \rangle=6 k_B T \gamma \delta_{ij} \delta(t-t')
 \end{equation}
%
  
  %
 \begin{figure}[t]
%\includegraphics[width=0.98\linewidth]{fig1.pdf}
\caption{A schematics of  tangentially-driven active polymer model }
\label{fig1} 
\end{figure}
%

\subsection{Units and simulation parameters}

We report the distances in length unit  of $\sigma$ and energy in units of $k_B T$.  The time is measured in unit of  $\tau=\sqrt{m \sigma^2/k_B T} $ where the mass of all beads is set to $m=1$.
Other simulations parameters are
chosen as  $ \epsilon/k_B T=1$ and $k_s = 50 k_B T/ \sigma^2$ making the chains effectively unstretchable. The maximum bond length is set to $R_0=1.5 \, \sigma$. These parameters lead to $\ell_b=\langle \vec{b} ^2 \rangle \approx \sara{0.964} \sigma$ for the average length of the bond vector $\vec{b}$, which ensures that filaments do not intersect. %The parameter choice is done such that the local filament curvature low such that the bead discretization does not violate the worm-like chain description.
We carry out  Langevin dynamics simulations of active polymers using Hoomd-blue ~\cite{hoomd1,hoomd2} with an in-house modification to include the active propulsion force. The  time step of integration is set to $\Delta t=0.01 \, \tau$ and the friction coefficient is chosen as  $\gamma=2 \, \tau^{-1}$.

In our simulations, we keep  the volume fraction constant  while varying the degree of polymerization $N$, bending stiffness $\kappa$  and strength of active force $f^a$.  The starting polymer configurations are  prepared by generating a random ensemble of  $n_c$ (number of chains) self-avoiding random walks composed of  $N$ beads  with an initial density of $\rho \sigma^3=0.85$ corresponding to $\phi=0.445$.  This density is identical to $\rho \sigma^3=0.85$ used in simulations of conventional polymer melts of Kremer-Grest model.  A typical configuration contained $ 6-10 \times 10^5$  beads in a cubic simulation box  of size $L_{\text{box}} = 89-105.5 \sigma$. 
 To remove the interchain bead overlaps, we initially switch on soft core potential employing the dissipative particle dynamics~\cite{DPD1,DPD2} followed by a conjugate-gradient minimization.  Then, we use a slow push off method in which initially  a very weak wAC potential with $\sigma=1$ and a very small $\epsilon=10^{-2}$ is switched on. Next, the  strength of WCA potential is gradually  increased  to it final value $\epsilon=1$.   
  After removing the overlaps, we equilibrate the configurations  following the Langevin dynamics  described by Eq.~\eqref{eq:Langevin} with $f^a=0$ for $10^3$-$10^4 \, \tau$ depending on the chain length as the equilibration time is $ \tau_{\text{eq}}\propto N^2$ for unentangled chains. The relaxed configurations are followed by a production runs of   $5 \times10^4 \, \tau$, where the active force is switched on.



%The polymer configurations for $N \le 500$ were equilibrated until the average  beads mean-square  displacement $\langle \Delta r^2(t) \rangle$ is equal  or larger than their mean square end-to-end distance $\langle R_e^2 \rangle$. The time for which $\langle \Delta r^2(t) \rangle=\langle R_e^2 \rangle$ is a measure of the relaxation time   and it is comparable to the Rouse time for the short chains and the disengagement time for the entangled chains \cite{polymerDoi} as will be verified in section V. 
% Table 1 provides a summary of configurations of polymers and the simulation time for equilibration  after push off stage  in units of $\tau$.  
 

\subsection{Dimensionless parameters of the model}
Our key parameters are the degree of polymerization,
$N \in \{10,30,50,100\}$, the filament  bending stiffness, $\kappa = 1 -20 \, k_B T$ , and
the magnitude of the active force,$ f^a = 0.1-10 k_BT/ \sigma$. 
As such, we can recognize two dimensionless parameters to charactrize the dynamics of active polymers.  The first one  describes the ratio of the active work relative to thermal energy we call it \emph{active Peclet number} 
%
\begin{equation}
\label{eq:pec1}
Pe=\frac{f^a (N-1)\ell_{b}}{k_BT}=\frac{f^a L}{k_BT}
\end{equation}
%
 where $f_a$ is the active force strength, $\ell_{b}$ the average bond length and $L=(N-1) \ell_b$ is the contour length of the polymer. In the case of isotropic active Brownian particles, the Peclet number is found to control the onset of the motility induced phase separation. 
 
  An alternative way to define the Peclet number is to consider the ratio between diffusive and advective time scales for a Rouse-like polymer (short polymers with $N \lesssim N_e$, where $N_e$ is the entanglement length). The diffusion coefficient of the center of mass of a Rouse chain is given by~\cite{polymerDoi,Rubinstein}
 %
 \begin{equation}
D_{\text{cm}}=\frac{k_BT}{\gamma N}=   \frac{D_1}{N},
\end{equation}
%
where $D_1=k_BT/ \gamma $ is the diffusion coefficient of  a single monomer. The diffusive time scale $\tau_D$ of a polymer can be estimated as the time needed for a polymer diffuse a distance comparable to its contour length $L=(N-1) \ell_b$:
%
 \begin{equation}
\tau_D =\frac{L^2}{6D_{\text{cm}} }\sim \frac{L^3 \gamma} {\ell_b k_BT}
\end{equation}
%
 
 Next, we estimate the advective time scale $\tau_A$ for a tangentially-driven active polymer as the time needed to move a distance $L$ via self-propulsion. Let's consider a conformation where the chain is straight. Every monomer feels a force $f^a$ along the end-to-end vector, tangential to the bond vectors. The  advective velocity uptake of each monomer  due to the active force and hence the whole chain  is $v^a=f^a/\gamma$. Therefore, the advective time scale is given by $\tau_A=L/v^a=L \gamma/f^a$. As a result, the Peclet number can be estimated as
 
 \begin{equation}
 \label{eq:pec2}
Pe =\frac{\tau_D}{\tau_A} \sim \frac{L^2 f^a} {\ell_b k_BT},
\end{equation}
 which differs from Eq.~\eqref{eq:pec1} by a factor of $L/\ell_b$.
 
 The second dimensionless group is the ratio of passive persistence length $\ell_{p}$ to the filament contour length $L$.
The passive persistence length  is obtained in terms of bending stiffness $\kappa$ as $ \ell_{p}= \frac{\kappa \ell_{b}}{k_BT}$. 
\begin{equation}
 \ell_{p}/L =\frac{ \kappa \ell_b}{L k_BT}=\frac{ \kappa}{(N-1)k_BT},
  \end{equation}
  As previously shown, the flexure number $\mathcal{F}$ which controls   the buckling and spiral instabilities, spontaneous spiral formation and rotational motion of a single active filament can be written in terms of the two  dimensionless numbers:
  \begin{equation}
  \mathcal{F}= \frac{Pe L}{\ell_{p}}=\frac{f^a L^3}{ \kappa},
    \end{equation}
 %
 where, we have use the Peclet number defined as in Eq.~\ref{eq:pec2}.
 Note that these definitions are slightly different from those used in Refs.~\cite{Isele,Prathyusha,active_priodic_force}.
 
%In view of new model where the force only acts on two bonds compared to homogenous self-propulsion model, I think it makes sense to define a modified Peclet number as $Pe=\frac{f_a \ell_{b0} N_{active}}{(N -1)k_BT}$  where $N_{active}$ is the number of active bond and $N-1$ is the total number of bond in the system.

\section{Observable to be computed from simulations}

\iffalse
\section{Conformational properties }

\subsection{scaling behavior of gyration and end-to-end distance} 

The mean square end-to-end distance is defined as
%
\begin{equation}
 \langle R_e^2 \rangle=\frac{1}{n_c}  \sum_{i=1}^{n_c} \langle(\mathbf{r}_{i,N}-\mathbf{r}_{i,1})^2 \rangle
\end{equation}
and the mean square gyration radius  is given by
\begin{equation}
 \langle R_g^2 \rangle=\frac{1}{n_c N}  \sum_{i=1}^{n_c} \langle \sum_{n=1}^{N} (\mathbf{r}_{i,n}-\mathbf{r}_{i,cm})^2 \rangle
\end{equation}
%
where $\mathbf{r}_{i,n}$ is the position of  $n$th bead of chain number $i$ and $\mathbf{r}_{i,cm}$ is the center of mass position  of the $i$th polymer chain in a sample. To have a good ensemble average,    an averaging over $50-200$ independent equilibrated chain configurations is required. 
 
It will be interesting to investigate


\begin{enumerate}
\item the dependence of $R_g$ and $R_e$ on $Pe$ for different bending stiffness will be interesting to explore.
\item  chain-length dependence of the mean square end-to-end distance and the gyration radius for chain sizes in the range $10\le N \le 500$ at different activity strengths $Pe$ and a few  $\kappa$  values  
 \end{enumerate}
\subsection{ Intrachain correlations}
  %
%
The  intrachain correlations for both bead positions and bond orientations can give us useful information about conformation of active polymers and how they differ from passive polymers and other active polymer models. \\

\noindent {\bf Mean-square internal distance (MSID)} \\
To quantify the positional intrachain correlations,  we calculate the mean-square internal distances (MSID) for various chain lengths  defined as 
\begin{equation}
 \langle R^2(n) \rangle=\frac{1}{n_c} \left\langle \sum_{i=1}^{n_c}  \sum_{j=1}^{N-n} \frac{1}{N-n}(\mathbf{r}_{i,j}-\mathbf{r}_{i,j+n})^2 \right\rangle
\end{equation}
%
where $n$ is the curvilinear (chemical) distance between the $j$th bead and
the $(j+n)$th bead along the same chain. $n_c$ here is the number of independent chain configurations used for averaging. Typically, at least 100 chain configurations are required to have reliable statistics.   MSID is a  measure of
internal chain conformation it is typically used to evaluate the equilibration degree of long polymers at equilibrium, but also gives us information about internal structure of polymers, \emph{i.e.} its conformation. For an ideal semiflexible polymer chain, $\langle R^2(n) \rangle/ (n  \ell_b^2)$ should be become  a constant defining the Flory's characteristic ratio $C_{\infty}$, where $ \ell_b^2= \left\langle (\mathbf{r}_{i+1}-\mathbf{r}_{i})^2 \right\rangle$.  
  is  the squared mean bond length. 
  
  
  From the asymptotic behavior of mean square end-to-end distances of long ideal chains, we can extract their  characteristic ratio $C_{\infty}$ and  Kuhn  length $\ell_K$.   The characteristic ratio is
defined by the relation   $\langle R_e^2(N) \rangle= C_{\infty} (N-1) \ell_b^2$, where $\ell_b$ is the average bond length; $ \ell_b^2= \left\langle (\mathbf{r}_{i+1}-\mathbf{r}_{i})^2 \right\rangle$.  
  The Kuhn length gives us the effective bond length of an equivalent freely jointed chain which has the same 
 mean square end-to-end distance $R_e^2$  and the same maximum end-to-end to distance $R_{\text{max}}$ \cite{Rubinstein}. For a freely jointed chain with $N_k$ Kuhn segments with  bond length $\ell_K$, we have
 $R_{\text{max}}=(N_k-1) \ell_k$ and $\langle R_e^2(N_k) \rangle=  (N_k-1)  \ell_k^2$. If $R_{\text{max}}=(N-1) \ell_b$ given that  $\langle R_e^2(N \gg 1) \rangle= C_{\infty} (N-1)  \ell_b^2$. 
  Equating $\langle R_e^2(N) \rangle$ and $R_{\text{max}}$  of  chains with those of the equivalent  freely jointed chain, we obtain $\ell_k=C_{\infty} \ell_b$ .
Comparing the mean square internal distance of  active  polymers with $\langle R^2(n) \rangle$ of the generalized freely rotating chain (FRC) model \cite{Flory,FRC} for ideal chains could be interesting. $\langle R^2(n) \rangle$ of  FRC  model only depends on the   value of $ \langle \cos \theta \rangle$ where  $\theta$ is   the  angle between any two successive bonds in
a chain. It is given by 
%
\begin{equation}
 \langle R^2(n) \rangle=n\ell_b^2 \left( \frac{1+\langle \cos \theta \rangle}{1-\langle \cos \theta \rangle}-\frac{2}{n} \frac{\langle \cos \theta \rangle (1-\langle \cos \theta \rangle^n)}{(1-\langle \cos \theta \rangle)^2}   
 \      \right) .
 \label{eq:Boltzmann}
\end{equation}
%

The   value of  $ \langle \cos \theta \rangle $ for the an equilibrium polymer model  can be obtained from $P_N(\theta) \propto \exp[-\beta U_{bend} (\theta)] $  as
\begin{equation}
 \langle \cos \theta \rangle=   \frac{\int_{0}^{\pi} \sin \theta \cos \theta \exp[-\beta U_{bend} (\theta)] }{\int_{0}^{\pi} \sin \theta  \exp[-\beta U_{bend} (\theta)]}.        
\end{equation}
%
where $U_{bend} (\theta)= \kappa (1-\cos (\theta)$ for typical semiflexible polymer chain model with $\kappa$ defining the bending stiffness.

   We can also  directly  infer $ \langle \cos \theta \rangle $ from  the MD simulations results as $ \langle \cos \theta \rangle \equiv \langle \widehat{\mathbf{b}}_{j} \cdot \widehat{\mathbf{b}}_{ j+1}  \rangle $ where  $\widehat{\mathbf{b}}_{j}$  is  the  $j$th unit bond vector   of a chain and the ensemble averaging shall be carried out over 100-500 equilibrated polymer conformations. \\


{\bf  Orientational bond-bond correlation  function   } \\

The intrachain  orientational bond-bond correlation function  is another useful quantity to characterise the intrachain correlations. It is  also known as  tangent-tangent correlation function and is defined as
\begin{equation}
 \langle \cos \theta  (n) \rangle \equiv \langle \widehat{\mathbf{b}}_{j} \cdot \widehat{\mathbf{b}}_{j+n}  \rangle,
 \end{equation}
    where $n$ is the curvilinear distance (chemical distance) between two bonds in a chain and $\widehat{\mathbf{b}}_{j}$ is the unit bond vector defined as $\widehat{\mathbf{b}}_{j}=(\mathbf{r}_{j+1}-\mathbf{r}_{j })/ |\mathbf{r}_{j+1}-\mathbf{r}_{j }|$.    
    We expect this function to be independent of $j$ for a sufficiently long chain or ideal chains.   We  can  extract the so-called persistence length $\ell_p$ from the bond-bond orientational correlation functions.  $\ell_p$ 
is defined by the initial decay length, more precisely:
\begin{equation}
  \langle \widehat{\mathbf{b}}_{j} \cdot \widehat{\mathbf{b}}_{j+n}  \rangle= \exp(-n \ell_b/\ell_p).
  \end{equation}
  %
 Notably,  for worm-like polymer chain model at equilibrium, \emph{i.e.} the limit of freely rotating chain model with  $\theta \ll 1$, the relation $\ell_k =2\ell_p$  roughly holds between persistence lenth and Kuhn length.  For large $n$ values of very long chains, the  bond-bond correlations  might deviate from exponential decay  and show a long-range power law decay  due to excluded volume effects.  A power law decay will be therefore a signature of long-range chain self-interactions. 

Another useful quantity in some contexts is  $\langle P_2( \cos \theta(n)) \rangle$. For the FRC theory, $\langle P_2( \cos \theta(n)) \rangle$ should also decay exponentially as 
   $ \langle P_2( \cos \theta(n)) \rangle= \langle P_2( \cos \theta) \rangle^n$ \cite{Porod1953}.
   
   \iffalse   
In polymer melts, $ \langle P_2( \cos \theta(n)) \rangle$ is typically proportional to the return probability of bead after  $n$ bonds
    \cite{Meyer2010}  that we denote by  $p_{\mathrm{ret}}(n)$. More precisely
        \begin{equation}
\langle P_2( \cos \theta(n)) \propto p_{\mathrm{ret}}(n) \equiv \lim_{\bR(n) \rightarrow 0} \Psi [\bR(n)] 
\label{eq:P_r(n)}
\end{equation}
   where  $\Psi [\bR(n)]$ is the probability distribution  function  of  $\bR(n)$ $\it{i. e.}$  the end-to-end vector  of all the chain segments (subchains) with  $n$ bonds.
   \fi
   
   
\subsection{Intrachain distribution functions}
   
    Let us first  consider the probability distribution function of bond length $P_N (b)$ corresponding to $R(n=1)$.
The distribution of bond length  should be independent of the chain length $N$ and depends on the form of bond potential chosen.    
For a harmonic bond potential, the normalized distributions of bond length $b$ can be well described by a  Gaussian distribution of the form
%
\begin{equation}
P_N (b)=    \frac{  \exp[ -\frac{(b-  \ell_{b0})^2}{2  \sigma_{b0}^2} ]}{\sqrt{2\pi \sigma_{b0}^2} },
\label{eq:Gaussianlb}
\end{equation}
%
in which $\sigma_{b0}$ represents the  standard deviation and the peak value $\ell_{b0}$ is the equilibrium bond length.
   
   For an  ideal  self-similar chain  $\Psi [\bR(n)]$, the probability distribution  function  of  $\bR(n)$ $\it{i. e.}$  the end-to-end vector  of all the chain segments (subchains) with  $n$ bonds for any subchain of size $n$ follows a Gaussian distribution of the form 
  \begin{equation}
 \Psi_{\mathrm{Gauss}} [\bR(n)]=  \left(\frac{3}{2 \pi \langle  R^2(n)\rangle} \right)^{3/2} \exp \left(-\frac{3 \bR^2(n)}{2 \langle  R^2(n) \rangle}\right)
 \label{eq:Gaussian0}
\end{equation}
where  $\langle R^2(n)\rangle \sim n^{2 \nu}$ and $\int_0^{\infty} 4 \pi R^2  \Psi (\bR)  dR =1$ \cite{Rubinstein,polymerDoi}.
%Hence, the return probability scales as $p_{\mathrm{ret}}(n) \sim n^{-3 \nu} =  n^{-3/2} $. 
For semiflexible  polymers with local correlations due to bending potential, we expect that $\Psi [\bR(n)]$ follows Eq. \ref{eq:Gaussian0} for $n \gg 1$.
 Denoting   the distance between any pair of beads $i$ and $j$ that are  $n \equiv |i-j|$ bonds apart by $R(n)$, 
 the  corresponding probability distribution function  for the reduced internal distances, $r(n)=R(n)/\sqrt{\langle R^2(n) \rangle}$,   follows from
\begin{equation}
%P_N(R_e)=4 \pi R_e^2 \left(\frac{3}{2 \pi \langle R_e^2\rangle} \right)^{3/2} \exp \left(-\frac{3 R_e^2}{2 \langle R_e^2 \rangle}\right)
P_{\mathrm{Gauss}}(r)=4 \pi r^2 \left(\frac{3}{2 \pi } \right)^{3/2}  \exp(-3 r^2/2 \langle r^2 \rangle)  %\frac{-3 r^2}{2 \langle r^2 \rangle} 
\label{eq:Gaussian}
\end{equation}
where $\int_{0}^{\infty} P_{\mathrm{Gauss}} (r) dr =1$.
 Particularly, one expects that the distribution functions of the end-to-end distance of sufficiently long chains should follow this distribution.

 
 {\bf Average torsional order parameter}
 This is a useful quantity for active polymers when in helical state and it  characterises the out-of-plane motion and twist of the filament in the helical phase. It  was introduced in \cite{helical_polymer}.
\begin{equation}
 U_t=\langle \sum_{i=2}^{N-1}\frac{1}{N-3} \cos \theta_i \rangle
\label{eq:twist}
\end{equation}
  where $\cos \theta_i $ is the torsion angle given by
\begin{equation}
 \cos \theta_i=\frac{(\vec{r}_{i-1} \times \vec{r}_{i} ) \cdot (\vec{r}_{i} \times \vec{r}_{i+1} ) }{|\vec{r}_{i-1} \times \vec{r}_{i} |   |\vec{r}_{i} \times \vec{r}_{i+1}|}
\label{eq:twist}
\end{equation}

\subsection{ Form factor of chain}
 
A  common way to characterize the structural  properties of polymers is to explore their  form factor, basically structure factor of a single chain  denoted by $S_c(q)$,   that can be measured directly in the scattering experiments.
The structure factor encompasses the information about spatial correlations between the beads via Fourier transform of  density-density correlation functions.    For spatially homogeneous and isotropic 
systems, the static structure factor only depends on the modulus $q$ of the wave vector.
The static structure factor $S_c(q)$  measured in scattering experiments    is  often spherically averaged over all the wave vectors $\mathbf{q}$ with the  same modulus $q$.  This quantity can be
computed as 
%
\begin{equation}
S_c(q)= \frac{1}{N n_c} \sum_{i=1}^{n_c} \sum_{n,m=1}^{N} \langle \exp \left[-i \mathbf{q} \cdot (\mathbf{r}_{i,n} -\mathbf{r}_{i,m}) \right] \rangle\label{eq:SQ1}
 \end{equation}
%
includes the contributions from intrachain pair correlations and it is called  intrachain or single chain structure factor. Equivalently,  $F(q)=S_c(q)/N$ known as the form factor \cite{Rubinstein} is used to quantity the intrachain correlations in the Fourier space.    
 
 
  The form factor of Gaussian chains,  known as  the  Debye function, is described by \cite{Rubinstein}
  %
  \begin{equation}
F_{Debye}(q)= \frac{2}{Q^2}  \left[ \exp(-Q)+Q-1   \right]   \quad \text{with} \quad  Q=q^2 \langle R_g^2\rangle
\label{eq:SQDebye}
 \end{equation}
%

  This is indeed the Fourier transform of the pair correlation function $g(r)$ of the beads.
  
  
 %Indeed, it has been shown that the form factor of short active chains $N\le 100$ can
 %be well described by  Koyama distribution for semiflexible polymers that expresses the site-resolved form factors to the second and forth moments of internal distances \cite{Vettorel2007}.
 
 
  
%

%

 

 \section{Dynamic scaling of polymer motion}
 
End-to-end vector correlation function 
 %
  \begin{equation}
 C_e(t)=\frac{\langle \vec{R}_e(t) \cdot \vec{R}_e(0) \rangle} {\langle \vec{R}_e^2(0) \rangle}
\label{eq:ct}
 \end{equation}
%
This is a very useful quantity that gives us an idea about the longest relaxation time of the chain. It is expected to follow roughly $C_e(t)\approx \exp(-t/\tau_r)$.

Similarly, to obtain the relaxation time of  a subchain of length $n$, one can compute 

 \begin{equation}
 C_n(t)=\frac{\langle \vec{R}(n,t) \cdot \vec{R}(n,0) \rangle} {\langle \vec{R}^2(n,0) \rangle}
\label{eq:ct}
 \end{equation}
%

Other useful dynamical quantities are the bead and centre of mass displacements.

i) the mean square displacement of inner beads  
\begin{equation}
 g_1(t)=\frac{1}{n_c (N/2+1)}  \sum_{i=1}^{n_c} \sum_{j=N/4}^{3N/4} \langle [\mathbf{r}_{i,j}(t)-\mathbf{r}_{i,j}(0)]^2 \rangle 
\end{equation}
where only the beads in the central region of a chain are considered to suppress the fluctuations caused by the chain ends.  \\
ii) the mean square displacement of inner beads with respect to
the corresponding center of mass (c.m.)  obtained as:
\begin{equation}
 g_2(t)=\frac{1}{n_c N}  \sum_{i=1}^{n_c} \sum_{j=1}^{N} \langle [(\mathbf{r}_{i,j}(t)-\mathbf{r}_{i,cm}(t))-(\mathbf{r}_{i,j}(0)-\mathbf{r}_{i,cm}(0))]^2 \rangle 
\end{equation}
  iii) the mean square displacement of the center of mass of chains defined as:
\begin{equation}
 g_3(t)=\frac{1}{n_c}  \sum_{i=1}^{n_c}  \langle [\mathbf{r}_{i,cm}(t))-\mathbf{r}_{i,cm}(0))]^2 \rangle 
\end{equation}

For short chains, it will be of interest to compare the dynamics with that of  the Rouse  theory, which is a Gaussian polymer model. It absorbs  all the interchain interactions  into a beadic friction and a coupling to a heat bath. The dynamics of the chain can then be described by a Langevin equation with noise and the constraint that the beads
are connected to form a chain. This description is known as the Rouse random walk model \cite{polymerDoi,Rubinstein}.
Beyond the microscopic timescale $\tau_0$, the Rouse model predicts that both  $ g_1(t)$ and $g_2(t)$  scale as $\sim t^{1/2}$ for timescales $ \tau_0< t < \tau_R $
in which  $\tau_R = \tau_0 N^2$ is known as  the Rouse time. It corresponds to the longest relaxation time of  polymers.  At longer times corresponding to
$t >\tau_R $, $ g_1(t)$ is expected to follow the Fickian diffusion and scale as $t$ whereas $g_2(t)$ is predicted to exhibit a plateau.  The mean-square displacement of chain's center of mass is predicted to be diffusive $ g_3(t)=6 D_{\text{cm}} t$ at all  the times $t > \tau_0 $ 
  with the diffusion coefficient scaling as $D_{\text{cm}} \sim  1/ N$. 
 
 
 Similar to the single chain structure factor, one can define a dynamical structure factor
 
 \begin{equation}
S_c(\vec{q},t)=  \frac{1}{N } \left \langle  \sum_{n,m=1}^{N} \exp \left[-i \mathbf{q} \cdot (\mathbf{r}_{n}(t) -\mathbf{r}_{m}(0)) \right] \right \rangle 
\label{eq:SQt}
 \end{equation}
%

 
  \fi

  
 
  
 \bibliography{active_polymer.bib}


 
\end{document}






 











