#! /usr/bin/env python3
import numpy as np, argparse

import pandas as pd
import freud
import gsd, gsd.hoomd

###############

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '*'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def get_pols(box, snap, pol_size=20):
    N_pols = int(snap.particles.N/pol_size)
    return np.array(np.split(box.unwrap(snap.particles.position,
                    snap.particles.image), N_pols))


def get_rcm(box, snap, pols=None, pol_size=None):
    return np.mean(pols[0:], axis=1)


def get_msid(box, snap, pols=None, pol_size=None):
    from scipy.spatial.distance import pdist, squareform
    pol_dist = np.array([pdist(p0)**2 for p0 in pols])
    mean_pol_dist = np.mean(pol_dist, axis=0)
    mean_pol_dist_matrix = squareform(mean_pol_dist)
    res = np.array([mean_pol_dist_matrix.diagonal(k).mean() for k in
                    np.arange(1, len(mean_pol_dist_matrix))])
    return res

def get_bondbond_orientional(box, snap, pols=None, pol_size=None):
    from scipy.spatial.distance import pdist, squareform
    # first get the bond vectors
    bonds = np.diff(pols, axis=1)
    bonds_dist = np.array([1. - pdist(b0, 'cosine') for b0 in bonds])
    mean_bonds_dist = np.mean(bonds_dist, axis=0)
    mean_bonds_dist_matrix = squareform(mean_bonds_dist)
    res = np.array([mean_bonds_dist_matrix.diagonal(k).mean() for k in
                    np.arange(1, len(mean_bonds_dist_matrix))])
    return res
###############


if __name__ == '__main__':
    func_dict = {
        'msid': get_msid,
        'bondbond': get_bondbond_orientional
    }
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input gsd file')
    parser.add_argument('-o', type=str, help='output filename')
    parser.add_argument('--output-type', help='either hdf or tsv',
                        default='tsv', dest='output_type')
    parser.add_argument('--mode', type=str, help='msid, bondbond or all', dest='mode', default='msid')
    parser.add_argument('--pol-size', type=int, default=20, help='polymer size', dest='pol_size')
    parser.add_argument('--t1', type=float, default=0.2)
    parser.add_argument('--t2', type=float, default=1.0)
    parser.add_argument('--step', type=float, default=0)
    args = parser.parse_args()

    # read the input file
    traj = gsd.hoomd.open(args.i, 'rb')
    box = freud.box.Box.from_box(traj[0].configuration.box)

    # read positions
    start = int(len(traj)*args.t1)
    end = int(len(traj)*args.t2)
    if args.step<=0:
        step = 1
    else:
        step = int(args.step*(end-start))
    print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))

    # prepare the result dict
    res = dict()
    quants = func_dict.keys()#['rcm', 'vcm', 'rg2', 're2', 'msid']
    for k in quants:
        res[k] = pd.DataFrame()
    #
    # pos = list()
    idummy = 0
    printProgressBar(idummy, end-start, prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    for t0 in traj[start:end+step:step]:
        pols = get_pols(box, t0, args.pol_size)
        for k in func_dict.keys():
            if ((k in args.mode.lower()) or ('all' in args.mode.lower())):
                quant_res = func_dict[k](box, t0, pols, args.pol_size)
                row_res = [((i+1), quant_res[i]) for i in
    np.arange(len(quant_res))]  # i+1 is the contour distance.
                row = [('timestep', t0.configuration.step)] + row_res
                res[k] = res[k].append(dict(row), ignore_index=True)
        printProgressBar(idummy*step, (end-start), prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
        idummy += 1
    for k in res.keys():
        if res[k].shape[0] > 0:
            res[k] = res[k].astype({'timestep': 'int64'})
            res[k].set_index('timestep', inplace=True)
            if args.output_type.lower()=='hdf':
                res[k].to_hdf(args.o+'.hdf', key=k)
            else:
                res[k].to_csv(args.o+'-%s.tsv'%k, sep='\t')
