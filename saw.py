import numpy as np
import random


def pbc(r, D):
    Dhalf = 0.5*D
    res = r
    system_dims = r.shape[0]
    for e in range(0, system_dims):
        if res[e] < -Dhalf[e]:
            res[e] = res[e] + D[e]
        elif res[e] >= Dhalf[e]:
            res[e] = res[e] - D[e]
    return res

###############################################################
###############################################################
# 2D polymer


def get_init_saw_xy(N=10, Nbinders=0, sigma=1.0, step_mag=1.0, no_backfolding_dist=1.02, D=0):
    #file_pol=open('initial_polymer.xyz', 'w')

    random.seed()
    R = 0.5*sigma  # beads radius
    if D == 0:
        D = 2*sigma*N**0.5887
    walk = np.zeros(shape=(N, 2))  # the walk chain!
    img = np.zeros(shape=(N, 2))
    # generate random chain:
    n = 0
    while (n < N):
        n = 0
        nmolecule = 1
        # random starting point for the chain in the box
        r1 = np.array([0.0, 0.0])
        r2 = np.array([random.random(), random.random()])*D
        # store 1st monomer of chain
        # ist monomer is always in original box (image = 0)
        r2 = pbc(r2, D)
        walk[n] = r2
        img[n] = np.array([0, 0])
        n = n+1
        # generate rest of monomers in the chain
        for imonomer in range(1, N):
            r0 = r1
            r1 = r2
        # random points inside sphere of unit radius
            accept = False
            counter = 0
            while (accept == False):
                if counter > 50:  # to restart constructing the polymer if there has been many fail attempts
                    break
                counter = counter+1
                r = 2.0
                while (r > 1.0):
                    rinner = np.array(
                        [random.uniform(-1, 1), random.uniform(-1, 1)])
                    r = np.linalg.norm(rinner)

                accept = True
                # project point to surface of sphere of unit radius
                rsurf = rinner / r
                # create new point by scaling unit offsets by bondlength (sigma = 1.0)
                r2 = r1 + rsurf*step_mag
                # check that new point meets restriction on backfolding
                dr = r2 - r0
                r = np.linalg.norm(dr)
                # check the overlap to previous beads
                if (imonomer > 2):
                    if (r <= no_backfolding_dist):
                        # print "backfolded->reject", imonomer
                        accept = False
                    else:
                        for ii in range(0, imonomer-1):
                            #rtemp=r2 - (walk[ii]+D*img[ii])
                            rtemp = pbc(r2, D)-walk[ii]
                            # real distance
                            # larger than sigma=2Ro to make sure there is no crossing
                            if (np.linalg.norm(rtemp) < sigma*1.3):
                                # print "overlap with preveous beads->reject ", imonomer
                                accept = False
                                break
            if counter > 50:
                break
            # store new point
            # if delta to revious bead is large,  then increment/decrement image flag
            r2 = pbc(r2, D)  # need to be coded
            walk[n] = r2
            dr = walk[n] - walk[n-1]
            for e in range(0, 2):
                if abs(dr[e]) < 2.0*step_mag:
                    img[n, e] = img[n-1, e]
                elif dr[e] < 0.0:
                    img[n, e] = img[n-1, e] + 1
                elif dr[e] > 0.0:
                    img[n, e] = img[n-1, e] - 1
            if (counter < 50 and accept == True):
                n = n + 1
            #file_pol.write(str(imonomer)+" "+str(walk[imonomer][0])+" "+str(walk[imonomer][1])+"\n")

    # file_pol.close()
    return walk, img
###############################################################
###############################################################
# 2D polymer


def get_init_pol_xy(N=100, Nbinders=0, sigma=1.0, step_mag=0.97, no_backfolding_dist=1.02, D=0):
    random.seed()
    R = 0.5*sigma  # beads radius
    walk = np.zeros(shape=(N, 2))  # the walk chain!
    img = np.zeros(shape=(N, 2))
    # generate random chain:
    n = 0
    nmolecule = 1
    # random starting point for the chain in the box
    r1 = np.array([0.0, 0.0])
    r2 = np.array([random.random(), random.random()])*D
    # store 1st monomer of chain
    # ist monomer is always in original box (image = 0)
    r2 = pbc(r2, D)
    walk[n] = r2
    img[n] = np.array([0, 0])
    n = n+1
    # generate rest of monomers in the chain
    for imonomer in range(1, N):
        r0 = r1
        r1 = r2
        # random points inside sphere of unit radius
        accept = False
        while (accept == False):
            r = 2.0
            while (r > 1.0):
                rinner = np.array(
                    [random.uniform(-1, 1), random.uniform(-1, 1)])
                r = np.linalg.norm(rinner)

            accept = True
            # project point to surface of sphere of unit radius
            rsurf = rinner / r
            # create new point by scaling unit offsets by bondlength (sigma = 1.0)
            r2 = r1 + rsurf*step_mag
            # check that new point meets restriction on backfolding
            dr = r2 - r0
            r = np.linalg.norm(dr)
            if (imonomer > 2) and (r <= no_backfolding_dist):
                accept = False
                # store new point
                # if delta to revious bead is large,  then increment/decrement image flag
            r2 = pbc(r2, D)  # need to be coded
            walk[n] = r2
            dr = walk[n] - walk[n-1]
            for e in range(0, 2):
                if abs(dr[e]) < 2.0*step_mag:
                    img[n, e] = img[n-1, e]
                elif dr[e] < 0.0:
                    img[n, e] = img[n-1, e] + 1
                elif dr[e] > 0.0:
                    img[n, e] = img[n-1, e] - 1
        n = n + 1
    return walk, img

###############################################################
###############################################################
# 2D circular polymer


def get_init_pol_2D_circ(N=100, sigma=1.0, step_mag=0.97, no_backfolding_dist=1.02, D=0):
    import freud
    box = freud.box.Box.square(D[0])

    def circ_points(r, n):
        phase0 = np.random.rand()*np.pi*2
        t = np.linspace(phase0, 2*np.pi+phase0, n)
        x = r * np.cos(t)
        y = r * np.sin(t)
        return np.c_[x, y, np.zeros(n)]
    walk = circ_points((N+1)*sigma/(2*np.pi), N+1)
    random_start = (np.random.rand(3)-0.5)*np.ones(3)*D[0]
    walk = walk[:-1] - random_start
    img = box.get_images(walk)
    return box.wrap(walk), img
###############################################################
###############################################################


def get_init_pol_xyz(N=100, sigma=1.0, step_mag=0.97, no_backfolding_dist=1.02, D=0, pbc_flag=np.ones(3)):
    random.seed()
    R = 0.5*sigma  # beads radius
    walk = np.zeros(shape=(N, 3))  # the walk chain!
    img = np.zeros(shape=(N, 3))
    # generate random chain:
    n = 0
    nmolecule = 1
    # random starting point for the chain in the box
    r1 = np.array([0.0, 0.0, 0.0])
    r2 = np.array([random.random(), random.random(), random.random()])*D
    # store 1st monomer of chain
    # ist monomer is always in original box (image = 0)
    r2 = pbc(r2, D)  # need to be coded
    walk[n] = r2
    img[n] = np.array([0, 0, 0])
    n = n+1
    # generate rest of monomers in the chain
    for imonomer in range(1, N):
        r0 = r1
        r1 = r2
        # random points inside sphere of unit radius
        accept = False
        while (accept == False):
            r = 2.0
            while (r > 1.0):
                rinner = np.array(
                    [random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1)])
                r = np.linalg.norm(rinner)

            accept = True
            # project point to surface of sphere of unit radius
            rsurf = rinner / r
            # create new point by scaling unit offsets by bondlength (sigma = 1.0)
            r2 = r1 + rsurf*step_mag
            # check that new point meets restriction on backfolding
            dr = r2 - r0
            r = np.linalg.norm(dr)
            if (imonomer > 2) and (r <= no_backfolding_dist):
                accept = False
            # check pbc:
            crossed_boundaries = np.abs(r2) > D*0.5
            accept = not(
                (pbc_flag.astype(int) - crossed_boundaries.astype(int)) == -1).any()

            #print(r2, pbc_flag, crossed_boundaries)

        # store new point
        # if delta to revious bead is large,  then increment/decrement image flag
        r2 = pbc(r2, D)  # need to be coded
        walk[n] = r2
        dr = walk[n] - walk[n-1]
        for e in range(0, 3):
            if abs(dr[e]) < 2.0*step_mag:
                img[n, e] = img[n-1, e]
            elif dr[e] < 0.0:
                img[n, e] = img[n-1, e] + 1
            elif dr[e] > 0.0:
                img[n, e] = img[n-1, e] - 1
        n = n + 1
    return walk, img

###############################################################
###############################################################


def rotate(x, y, r):
    rx = (x*np.cos(r)) - (y*np.sin(r))
    ry = (y*np.cos(r)) + (x*np.sin(r))
    return (rx, ry)


def point_ring(center, N, R):
    arc = 2*np.pi/N
    points = []
    for p in range(N):
        (px, py) = rotate(0, R, arc*p)
        px += center[0]
        py += center[1]
        points.append([px, py])
    return np.array(points)


def get_init_ring_pol_xyz(N=100, sigma=1.0, R0=1.05, no_backfolding_dist=1.02, D=0, pbc_flag=np.ones(3)):
    from scipy.spatial.transform import Rotation as Rot
    random.seed()
    r_vec = np.random.rand(2) * 2*np.pi
    #r_vec = r_vec/np.linalg.norm(r_vec) * np.random.rand()*np.pi*2
    rot = Rot.from_euler('yx', r_vec)
    Dhalf = 0.5*D[0]
    # R = 0.5*sigma  # beads radius
    gamma = np.pi * (0.5 - 1./N)
    R = R0/(2*np.cos(gamma))
    walk = np.zeros(shape=(N, 3))  # the walk chain!
    img = np.zeros(shape=(N, 3))
    center = np.random.rand(3)*D
    center = pbc(center, D)
    walk[:, :2] = point_ring(center, N, R)
    walk[:, 2] = center[2]
    walk = rot.apply(walk)
    for i in range(3):
        img[:, i] += (walk[:, i] < -Dhalf).astype(int) - \
            (walk[:, i] > Dhalf).astype(int)
    walk += D*img

    return walk, img


def unwrapped_positions(positions, img, D):
    Dhalf = D*0.5
    res = positions.copy()

    '''for i in range(0,3): # loop over different coordination components
        for j in range(0,len(positions)-1): #loop over the polymer
            r1=positions[j]
            r2=positions[j+1]
            dr=r1-r2
            if dr[i]>Dhalf:
                res[j+1:,i] = res[j+1:,i] + D
            elif dr[i]<-Dhalf:
                res[j+1:,i] = res[j+1:,i] - D'''
    for i in range(0, len(res)):
        res[i] = res[i] + img[i]*D

    return res


def R_G2(positions, img, D):

    pos = unwrapped_positions(positions, img, D)
    rcm = np.mean(pos, axis=0)
    res1 = (pos - rcm)**2
    result = res1.sum(axis=1).mean()
    return result


def R2(positions, img, D):
    pos = unwrapped_positions(positions, img, D)
    res = ((pos[0] - pos[-1])**2).sum()
    return res


def gen_lammps_coords(fname, N=100, Nbinders=0, sigma=1.0, step_mag=0.97, no_backfolding_dist=1.02, D=0):
    if D == 0:
        D = 2+1*N**0.5887
    Dhalf = D*.5
    # pos = get_init_saw_xyz(N, sigma, step_mag, no_backfolding_dist, D) # get the positions of particles inside the box
    # pos, img = get_init_polymer_xyz(N=N, sigma=sigma, step_mag=step_mag, no_backfolding_dist=no_backfolding_dist, D=D) # get the positions of particles inside the box
    # get the positions of particles inside the box
    pos, img = get_init_pol_xyz(
        N=N, sigma=sigma, step_mag=step_mag, no_backfolding_dist=no_backfolding_dist, D=D)
    output = open(fname, 'w')
    header = """
{0} atoms
{1} bonds\n
2 atom types
1 bond types\n
-{2} {2} xlo xhi
-{2} {2} ylo yhi
-{2} {2} zlo zhi\n\n""".format(N+Nbinders, N-1, Dhalf)
    #print (header)
    atom_line = '{0} {1} {2} {3} {4} {5} {6} {7} {8}\n'
    bond_line = '{0} {1} {2} {3}\n'
    atom_str = ''
    bond_str = ''
    for i in range(0, N):
        atom_str = atom_str+atom_line.format(i+1, 1, 1,
                                             pos[i, 0], pos[i, 1], pos[i, 2],
                                             img[i, 0],  img[i, 1],  img[i, 2])
        if i < N-1:
            bond_str = bond_str+bond_line.format(i+1, 1, i+1, i+2)

    for i in range(0, Nbinders):
        x = random.uniform(-Dhalf, Dhalf)
        y = random.uniform(-Dhalf, Dhalf)
        z = random.uniform(-Dhalf, Dhalf)
        atom_str = atom_str+atom_line.format(N+i+1, 2, 2, x, y, z, 0, 0, 0)

    text = header+'Atoms\n\n'+atom_str+'\nBonds\n\n'+bond_str
    output.write(text)
    output.close()
